package rizqi.khoirul.appx09t

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context:Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "music"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        val tmusik = "create table musik(id_music text primary key, id_cover text, musik_title text)"
        db?.execSQL(tmusik)
    }
}